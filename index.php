<!doctype html>
<html lang="en">
	<head>
  		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <title></title>
	    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
	    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	    <link rel="stylesheet" href="/resources/demos/style.css">
		<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
	    <style>	
	    	body{
	    		font-family: 'Tahoma';
	    		background-color: #f8f8ff;
				margin: auto;
	    	}    
	       .char{
	       		font-size: 7em;	 
	       }
	       .center {
	       		margin-left: auto;
  				margin-right: auto;
  				text-align: center;  				
	       }
	       .res {
	       		padding: 5px;
	       		width: 80px;
	       		height: 60px;
	       		text-align: center ;
	       		border-radius: 10%;
	       		background-color: white;
	       		border: 1px gray solid; 
	       		font-size: 36px;
	       		font-family: 'comic sans ms';
	       }

	       .res:focus{
	       	outline: none;
	       }
	       .resp {
				font-size: 2em;	 
	       		height: 30px; 
	       }
	       .wrong {
	       		background-color: #ff7676 ;
	       }

	       .right {
	       		background-color: #009d00 ;
	       }

	       .icon {
	       		border-radius: 10px;
	       		padding: 10px;
	       		height: 10px;
	       		width: 10px;
	       }

	       .hide {
				display:none;
	       }

	       .visible {
	       		visibility:block;
	       }
	       .red {
	       		font-size: 10px;
	       		border: 1px red solid ;
	       		color: red;
	       		padding: 3px;
	       		margin: 2px;
	       		float:left;
	       }

	       .red:hover {
	       		background-color: #ffebeb ;
	       		cursor: pointer;

	       }

	       .green {
	       		font-size: 10px;
	       		border: 1px green solid ;
	       		color: green;
	       		padding: 3px;
	       		margin: 2px;
	       		float:left;
	       }

	       input[type="button"]{
	       	padding: 5px;
	       	border-radius: 5px;
	       	width: 100px;
	       	margin-bottom: 15px;
	       	background-color: #b1b1ff;
	       	border: 1px black solid;
	       }

	       input[type="button"]:hover{	
	       cursor: pointer;       	
	       	background-color: #e4e4ff;	       
	       }

	       #history{
	       		float: left;
	       		margin-left: auto;
  				margin-right: auto;
  				text-align: center; 
	       		width: 100%;
	       		border: 1px gray solid;
	       		border-radius: 5px;
	       		padding: 10px;
	       		background-color: white;
	       }

	       table, table tr, table td{
				width: 100%;
	       }
	    </style>

	    <script>	
	    	var currentRomaji = "" ;
	    	var currentCarac = "" ;
	    	var currentRomajiId = 0 ;
	    	var currentRomajiRight = 0 ;
	    	var currentRomajiTotal = 0 ;

	    	document.getElementById('res').onkeypress = function(e){
			    if (!e) e = window.event;
			    var keyCode = e.keyCode || e.which;
			    if (keyCode == '13'){
			      testEquals();
			      return false;
			    }
			  }

	    	function getRandomChar(){

	    		$.ajax({
				    type: 'POST',				  
				    url: 'GetCarac.php',
				    dataType: 'html',
				    success: function(result){
				    currentCarac = result.split("|")[0];
				       document.getElementById("jap").innerHTML = currentCarac ;
				       currentRomaji = result.split("|")[1].trim() ;
				       currentRomajiId = Number(result.split("|")[2]) ;
				       currentRomajiRight = Number(result.split("|")[3]) ;
				       currentRomajiTotal = Number(result.split("|")[4]) ;
				       document.getElementById("ratio").innerHTML = currentRomajiRight + " / " + currentRomajiTotal ;
				       document.getElementById("res").value = "" ;
				       document.getElementById("resp").innerHTML = "" ;
				       document.getElementById("res").style.color = 'black' ; 	  

				       //document.getElementById("yes").className = 'hide' ; 
				       //document.getElementById("no").className = 'hide' ; 


				    },	
				    error: function(result){
				       document.getElementById("jap").innerHTML = "???" ;
				    }			   
				});
	    	}

	    	function testEquals(){
	    		var right = 0 ;


	    		if ((document.getElementById("res").value) === currentRomaji){
	    			//document.getElementById("yes").className = 'visible' ; 	   
	    			document.getElementById("res").style.color = 'green' ; 	  
	    			document.getElementById("history").innerHTML += "<div class='green'><b>" + currentCarac + "</b> - (" + currentRomaji + ") </div>";
					right = 1 ;
	    		} else {
	    			//document.getElementById("no").className = 'visible' ; 
	    			document.getElementById("ra").innerHTML = currentRomaji ;
	    			document.getElementById("res").style.color = 'red' ; 	   
	    			$.ajax({
						type: 'POST',
		    			url: 'GetOne.php',
		    			dataType: 'html',	
		    			data: {
		    				"romaji" : document.getElementById("res").value
		    			},
					    success: function(result){
	    					document.getElementById("history").innerHTML += "<div class='red' title='You said " + result + " - \"" + document.getElementById("res").value + "\"'><b>" + currentCarac + "</b> - (" + currentRomaji + ") </div>";
					    }, 
					    error: function(){
	    					document.getElementById("history").innerHTML += "<div class='red'><b>" + currentCarac + "</b> - (" + currentRomaji + ") </div>";
					    } 		    	   
					});

	    		}

	    		$.ajax({
					type: 'POST',
	    			url: 'CountCarac.php',
	    			dataType: 'html',	
	    			data: {
	    				"id" : currentRomajiId,
	    				"right" : right
	    			},
				    success: function(result){
				       document.getElementById("sql").value = result;
				    }, 
				    error: function(){
				       document.getElementById("sql").innerHTML = "error";
				    } 		    	   
				});
	    	}
	    
	    </script>

	</head>
	<body onLoad="getRandomChar()">
		<fieldset>日本語学習サイト SWAG</fieldset>
		<table>		
			<tr>
				<td>
					<div class="center">
						<div id="jap" class="char"></div>
						<div id="ratio"></div><br>
						<input id="res" class="res" type="text" autofocus/><div id="ra" class="resp"></div>
						<div id="yes" hidden><img id="yes" class="right icon" src="yes.png" ></div>
						<div id="no" hidden><img id="no" class="wrong icon" src="no.png" ></div><br>
						<input id="sub" class="sub" type="button" value="Répondre !" onClick="testEquals()"/><br>
						<input id="gen" class="gen" type="button" value="NEXT !" onClick="getRandomChar()"/><br><br>
						
						
					</div>	
				</td>
			</tr>
			<tr>
				<td>
					<div id="history"></div>
				</td>
			</tr>
		</table>
		
		
	</body>
</html>